import React from "react";
import {
  Card,
  Column,
  CardHeader,
  CardHeaderTitle,
  CardHeaderIcon,
  Field,
  Control,
  Button,
  Image,
  CardImage
} from "bloomer";
import { XkcdResponse } from "./Main";

interface Props {
  c: XkcdResponse;
  voteOnComic: (num: number, vote: -1 | 1) => any;
}
const ComicCard: React.SFC<Props> = props => {
  const { c, voteOnComic } = props;
  return (
    <Column isSize="1/3" key={c.num}>
      <Card>
        <CardHeader>
          <CardHeaderTitle>{c.title}</CardHeaderTitle>
          <CardHeaderIcon>
            <Field isGrouped>
              <Control>
                <Button
                  isColor={c.vote === 1 ? "success" : ""}
                  onClick={() => voteOnComic(c.num, 1)}
                >
                  UP
                </Button>
              </Control>
              <Control>
                <Button
                  isColor={c.vote === -1 ? "danger" : ""}
                  onClick={() => voteOnComic(c.num, -1)}
                >
                  DOWN
                </Button>
              </Control>
            </Field>
          </CardHeaderIcon>
        </CardHeader>
        <CardImage>
          <Image src={c.img} />
        </CardImage>
      </Card>
    </Column>
  );
};

export default ComicCard;
