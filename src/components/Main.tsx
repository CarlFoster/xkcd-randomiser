import React, { useState, useEffect } from "react";
import { Section, Container, Columns, Level, LevelItem, Button } from "bloomer";
import ComicCard from "./ComicCard";

const MAX_ID = 2081;
const NUM_OF_PANELS = 9;

export interface XkcdResponse {
  month: string;
  num: number;
  link: string;
  year: string;
  news: string;
  safe_title: string;
  transcript: string;
  alt: string;
  img: string;
  title: string;
  day: string;
  vote: number;
}

const getComics = async (): Promise<XkcdResponse[]> =>
  Promise.all(
    Array.from([...Array(NUM_OF_PANELS)])
      .map(_ => Math.floor(Math.random() * MAX_ID)) // Not true randomness but close enough
      // .map((_, i) => MAX_ID - i) // Test if statefulness works
      .map(num =>
        fetch(`/api/xkcd/${num}`)
          .then(res => res.json())
          .catch(_ => ({ num }))
      )
  );

const sendVote = (id: number, vote: 1 | -1) =>
  fetch(`/api/vote/${id}?direction=${vote}`, { method: "POST" }).then(res =>
    res.json()
  );

const Main: React.SFC = () => {
  const [comics, setComics] = useState<XkcdResponse[]>([]);
  const [isLoading, setLoading] = useState<boolean>(true);

  // Unsure how best to do this sort of thing with new hooks
  const voteOnComic = async (num: number, vote: 1 | -1) => {
    const { vote: returnVote } = await sendVote(num, vote);
    setComics(
      comics.map(comic =>
        comic.num === num ? { ...comic, vote: returnVote } : comic
      )
    );
  };
  const loadComics = () => {
    setLoading(true);
    setComics([]);
    getComics()
      .then(setComics)
      .then(() => setLoading(false));
  };

  // Passing in array ensures that this is run once
  useEffect(loadComics, []);
  return (
    <Container>
      <Section>
        <Level>
          <LevelItem>
            <Button
              onClick={loadComics}
              isLoading={isLoading}
              isColor="primary"
            >
              Reload
            </Button>
          </LevelItem>
        </Level>
        <Columns isMultiline>
          {comics.map(c => (
            <ComicCard c={c} voteOnComic={voteOnComic} />
          ))}
        </Columns>
      </Section>
    </Container>
  );
};

export default Main