import server, { router, reply } from "server";
import { Middleware, Context } from "server/typings/common";
import { get } from "https";

enum Votes {
  up = 1,
  down = -1,
  nothing = 0
}

interface SessionState {
  votes: Record<string, Votes>;
}


const main = () => server({ port: 8080, views: "public", engine: "none", security: false }, [
  router.get("/", c => reply.render("index")),
  router.get("/api/xkcd/:id", getXkcd),
  setVotes
]);

const getXkcd: Middleware = c =>
  fetchJson(`https://xkcd.com/${c.params.id}/info.0.json`).then(data => ({
    ...data,
    vote: getVote(c)
  }));

const fetchJson = (path: string) => {
  return new Promise((resolve, reject) => {
    let data = "";
    get(path, response => {
      response.on("data", chunk => (data += chunk));
    })
      .on("close", () => resolve(JSON.parse(data)))
      .on("error", reject);
  });
};

const getVote = (c: Context) => {
  const {
    session: { votes },
    params: { id }
  } = c as Context & { session: SessionState };
  return !votes || !votes[id] ? Votes.nothing : votes[id];
};


// This should be handled through a database
// instead of the in memory session object
// If this were more RESTy then this would be a
// PUT request with no query parameters
const setVotes = router.post("/api/vote/:id", (c: Context) => {
  const {
    params: { id },
    query: { direction }
  } = c;
  let { session } = c as { session: SessionState };
  session.votes = session.votes || {};

  const vote = parseVote(direction);
  session.votes[id] = session.votes[id] === vote ? Votes.nothing : vote;

  return reply.json({ vote: session.votes[id] });
});

const parseVote = (x: unknown) => {
  const numeric = Number(x);
  return isNaN(numeric) || numeric === 0
    ? Votes.nothing
    : numeric > 0
    ? Votes.up
    : Votes.down;
};

main()
