# XKCD Randomiser

## Running

```bash
npm run build
npm start
```
Accessible at `localhost:8080`

## Implementation
Naive random Xkcd comic fetcher with very basic session based voting.

The votes are sent via a simple POST request. There is no accumulation of votes, purely just the users vote on that comic.
Using server-side sessions ensures that if a user accesses the website from the same browser, then they will have their pre-existing vote.

To extend to having aggregated votes, the data store should look like:
```
| Comic ID | Session ID | Vote |
```

On GET of any comic would select both the `userVote` based on Session ID and the sum of all votes for that comic. An index could be applied on: `SUM(VOTE) GROUP BY COMIC_ID` to enable faster reads.

On voting, I would return the new `userVote` and the current `voteCount` and the client would update accordingly.

To handle API failures, the client could store all the new `userVote`s in session storage and periodically send requests to the server with those votes. The client would have the current `userVote` for the comic, and would simply increment or decrement the `voteCount` as necessary.  